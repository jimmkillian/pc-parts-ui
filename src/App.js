import React from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'

import './App.css'
import Component from './views/Components';
import BrandType from './views/BrandsTypes';
import ComponentEditor from './views/ComponentEditor';

const App = () => {
  return (
    <div>
      <Router>
        <nav>
          <div className='container'>
            <div className='row'>
              <div className='col'>
                <nav className='navbar navbar-expand-lg navbar-light bg-light'>
                  <a className='navbar-brand' href='/'>PC Parts 1.0</a>
                  <button
                    className='navbar-toggler' type='button' data-toggle='collapse'
                    data-target='#navbarNavAltMarkup' aria-controls='navbarNavAltMarkup'
                    aria-expanded='false' aria-label='Toggle navigation'
                  >
                    <span className='navbar-toggler-icon' />
                  </button>
                  <div className='collapse navbar-collapse' id='navbarNavAltMarkup'>
                    <div className='navbar-nav'>
                      <Link to='/' className='nav-link active'>Components</Link>
                      <Link to='/brandtype' className='nav-link active'>Brands &amp; Types</Link>
                    </div>
                  </div>
                </nav>
              </div>
            </div>
          </div>
        </nav>
        <Switch>
          <Route path='/componenteditor/:componentID'>
            <div className='display-frame'>
              <ComponentEditor />
            </div>
          </Route>
          <Route path='/brandtype'>
            <div className='display-frame'>
              <BrandType />
            </div>
          </Route>
          <Route path='/'>
            <div className='display-frame'>
              <Component />
            </div>
          </Route>
        </Switch>
      </Router>
    </div>
  )
}

export default App
