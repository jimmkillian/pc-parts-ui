import React, { useEffect, useState } from 'react';
import { getBrands } from '../services/brandService';
import { getComponentTypes } from '../services/componentTypeService';

// Displays list of Brands and Component Types in database
const BrandsTypes = () => {

    const [brands, setBrands] = useState([]);
    const [componentTypes, setComponentTypes] = useState([]);

    useEffect(() => {
        getBrands()
        .then((data) => {
            setBrands(data);
        })
        .catch((error) => {
            console.log(error);
        });
    }, []);
    
    useEffect(() => {
        getComponentTypes()
        .then((data) => {
            setComponentTypes(data);
        })
        .catch((error) => {
            console.log(error);
        });
    }, []);

  return (
    <div className="container">
        <div className="row">
            <div className="col-3">
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col-2">Brand Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {brands.map((brand) => (
                            <tr>
                                <td id={brand.brandID}>
                                    {brand.brandName}
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
            <div className="col-3">
            <table className="table">
                    <thead>
                        <tr>
                            <th scope="col-2">Component Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        {componentTypes.map((cType) => (
                            <tr>
                                <td id={cType.componentTypeID}>
                                    {cType.componentTypeName}
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
  )
};

export default BrandsTypes;
