import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { getComponents } from '../services/componentService';

// Displays list of components currently in database
// Each component can be edited (or deleted) by clicking the Edit link
// New component can be created by clicking the button
// Calls ComponentEditor to perform further operations
const Component = () => {

    const [components, setComponents] = useState([]);

    useEffect(() => {
        getComponents()
        .then((data) => {
            setComponents(data);
        })
        .catch((error) => {
            console.log(error);
        });
    }, []);
    
    return (
        <div className="container">
            <div className="row">
                <div className="col">
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">&nbsp;</th>
                                <th scope="col-2">Component Name</th>
                                <th scope="col-2">Component Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            {components.map((component) => (
                                <tr key={component.componentID}>
                                    <th scope="row">
                                        <Link to={`/componenteditor/${component.componentID}`} className='nav-link active'>
                                            Edit
                                        </Link>
                                    </th>
                                    <td>{component.componentName}</td>
                                    <td>{component.componentDescription}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
            <div className="row">
                <Link to={`/componenteditor/NEW`} className='nav-link active'>
                    <button id="btnNewComp" className="btn btn-success">New Component</button>
                </Link>
            </div>
        </div>
    )
};

export default Component;
