import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { v4 as guidv4 } from 'uuid';
import { getComponent, putComponent, postComponent, delComponent } from '../services/componentService';
import { getBrands } from '../services/brandService';
import { getComponentTypes } from '../services/componentTypeService';

// Allows editing or deleting of existing component records
// Allows creation of new component records
const ComponentEditor = () => {

    let params = useParams();
    const [brands, setBrands] = useState([]);
    const [componentTypes, setComponentTypes] = useState([]);

    const component = {
        componentID: 'NEW',
        componentName: '',
        componentDescription: '',
        componentPrice: '',
        brandID: '',
        componentTypeID: ''
    };

    const [currentComponent, setCurrentComponent] = useState(component);

    let [compName, setCompName] = useState(component.componentName);
    let [compDesc, setCompDesc] = useState(component.componentDescription);
    let [compPrice, setCompPrice] = useState(component.componentPrice);
    let [compBrand, setCompBrand] = useState(component.brandID);
    let [compType, setCompType] = useState(component.componentTypeID);

    useEffect(() => {
        if (params.componentID !== "NEW")
            getComponent(params.componentID)
            .then((data) => {
                setCurrentComponent(data);

                setCompName(data.componentName);
                setCompDesc(data.componentDescription);
                setCompPrice(data.componentPrice);
                setCompBrand(data.brandID);
                setCompType(data.componentTypeID);
            })
            .catch((error) => {
                console.log(error);
            });
        }, []);
    
    useEffect(() => {
        getBrands()
        .then((data) => {
            setBrands(data);
            if (params.componentID === "NEW")
                setCompBrand(brands[0]);
        })
        .catch((error) => {
            console.log(error);
        });
    }, []);
    
    useEffect(() => {
        getComponentTypes()
        .then((data) => {
            setComponentTypes(data);
            if (params.componentID === "NEW")
                setCompType(componentTypes[0]);
        })
        .catch((error) => {
            console.log(error);
        });
    }, []);
    
  return (
    <form>
        <div className="container">
            <div className="row">
                <div className="col-2" align="right">
                    <label htmlFor="txtName" className="form-label">Name:</label>
                </div>
                <div className="col-6">
                    <input type="text" id="txtName" className="form-control" 
                        value={compName} 
                        onChange={(event) => setCompName(event.target.value)} />
                </div>
            </div>
            <div className="row">
                <div className="col-2" align="right">
                    <label htmlFor="txtDesc" className="form-label">Description:</label>
                </div>
                <div className="col-6">
                    <input type="text" id="txtDesc" className="form-control"
                        value={compDesc} 
                        onChange={(event) => setCompDesc(event.target.value)} />
                </div>
            </div>
            <div className="row">
                <div className="col-2" align="right">
                    <label htmlFor="txtPrice" className="form-label">Price:</label>
                </div>
                <div className="col-6">
                    <input type="number" id="txtPrice" className="form-control"
                        value={compPrice} 
                        onChange={(event) => setCompPrice(event.target.value)} />
                </div>
            </div>
            <div className="row">
                <div className="col-2" align="right">
                    <label htmlFor="slctBrand" className="form-label">Brand:</label>
                </div>
                <div className="col-6">
                    <select id="slctBrand" className="form-control" 
                        value={compBrand} 
                        onChange={(event) => setCompBrand(event.target.value)}>
                        {brands.map((c) => (
                            <option key={c.brandID} value={c.brandID}>{c.brandName}</option>))}
                    </select>
                </div>
            </div>
            <div className="row">
                <div className="col-2" align="right">
                    <label htmlFor="slctType" className="form-label">Type:</label>
                </div>
                <div className="col-6">
                    <select id="slctType" className="form-control"
                        value={compType} 
                        onChange={(event) => setCompType(event.target.value)}>
                        {componentTypes.map((c) => (
                            <option key={c.componentTypeID} value={c.componentTypeID}>{c.componentTypeName}</option>))}
                    </select>
                </div>
            </div>
            <div className="row">
                <div className="col-2">
                </div>
                <div className="col-6">
                    <br />
                    {(currentComponent.componentID === 'NEW') ?
                        <div>
                            <button id="btnSaveComp" className="btn btn-primary"
                                onClick={() => saveCurrentComponent({
                                    componentID: currentComponent.componentID,
                                    componentName: compName,
                                    componentDescription: compDesc,
                                    componentPrice: parseFloat(compPrice),
                                    brandID: compBrand,
                                    componentTypeID: compType
                                })}>Create Component
                            </button>
                        </div>
                    :   
                        <div>
                            <button id="btnSaveComp" className="btn btn-success" 
                                onClick={() => saveCurrentComponent({
                                    componentID: currentComponent.componentID,
                                    componentName: compName,
                                    componentDescription: compDesc,
                                    componentPrice: parseFloat(compPrice),
                                    brandID: compBrand,
                                    componentTypeID: compType
                                })}>Save Component
                            </button>
                            &nbsp;
                            <button id="btnDelComp" className="btn btn-danger" 
                                onClick={() => deleteCurrentComponent(currentComponent.componentID)}>
                                Delete Component
                            </button>
                        </div>
                    }
                </div>
                <div className="col-3">
                    <br />
                    <Link to={`/`} className='nav-link active'>
                        <button id="btnBack" className="btn btn-secondary">
                            Back to Components
                        </button>
                    </Link>
                </div>
            </div>
        </div>
    </form>
  )
}

// Saves entered changes to component by calling postComponent(component) for
// new component or putComponent(component) for an existing component
const saveCurrentComponent = (component) => {

    console.log("SAVE:",component);
    if (component.componentID === 'NEW') {
        component.componentID = guidv4();
        postComponent(component)
        .then((data) => {
            console.log(data);
        })
        .catch((error) => {
            console.log(error);
        });
    }
    else {
        putComponent(component)
        .then((data) => {
            console.log(data);
        })
        .catch((error) => {
            console.log(error);
        });
    };
};

// Calls delComponent(componentID) to delete the currently loaded component
// from database
const deleteCurrentComponent = (componentID) => {

    console.log("DELETE:",componentID);
    delComponent(componentID)
    .then((data) => {
        console.log(data);
    })
    .catch((error) => {
        console.log(error);
    });
};

export default ComponentEditor;