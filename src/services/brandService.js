import axios from 'axios';

// Retrieves list of Brands from database
// Each record includes brandID and brandName
// List is ordered by brandName
const getBrands = async () => {
  const url = `https://localhost:5001/brands`;

  const response = await axios({
    method: 'get',
    url: url
  });

  return response.data
}

export { getBrands };
