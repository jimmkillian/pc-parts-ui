import axios from 'axios';

// Retrieves list of Components from database
// Each record includes componentID, componentName, and componentDescription
// List is ordered by componentName
const getComponents = async () => {
    const url = `https://localhost:5001/components`;

    const response = await axios({
        method: 'get',
        url: url
    });

    return response.data
};

// Retrieves full record of component specified by componentID
// Includes componentID, componentName, componentDescription, componentPrice,
//          brandID, componentTypeID
const getComponent = async (componentID) => {
    const url = `https://localhost:5001/components/${componentID}`;

    const response = await axios({
        method: 'get',
        url: url
    });

    return response.data
};

// Updates database record specified by componentID
const putComponent = async (component) => {
    const url = `https://localhost:5001/components/${component.componentID}`;

    const response = await axios({
        method: 'put',
        url: url,
        data: component
    });

    console.log("PUT:", component, response.data);
    return response.data
};

// Inserts new component record in database
const postComponent = async (component) => {
    const url = `https://localhost:5001/components/`;

    try {
        const response = await axios({
            method: 'post',
            url: url,
            data: component
        });

        console.log("POST:", component, response.data);
        return response.data
    } catch (error) {console.log(error)};
};

// Deletes component record from database specified by componentID
const delComponent = async (componentID) => {
    const url = `https://localhost:5001/components/${componentID}`;

    const response = await axios({
        method: 'delete',
        url: url
    });

    return response.data
};

export { getComponents, getComponent, putComponent, postComponent, delComponent };
