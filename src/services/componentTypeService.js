import axios from 'axios';

// Retrieves list of componentTypes from database
// Each record includes componentTypeID and componentTypeName
// List is ordered by componentTypeName
const getComponentTypes = async () => {
  const url = `https://localhost:5001/componentTypes`;

  const response = await axios({
    method: 'get',
    url: url
  });

  return response.data
};

export { getComponentTypes };
